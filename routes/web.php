<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/install', function () {
    \Illuminate\Support\Facades\Artisan::call('migrate:fresh', [
        '--seed' => true
    ]);
    \Illuminate\Support\Facades\Artisan::call('passport:install');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth', 'role:super_admin|school_admin'])
    ->namespace('Backend')->prefix('admin')->name('admin.')->group(function () {

        Route::resource('/dashboard', 'DashboardController');

        Route::resource('/user', 'UserController');
        Route::post('/user/search', 'UserController@search')->name('user.search');

        Route::get('/user/assignSchoolModal/{entryID?}', 'UserController@assignSchoolModalIndex')
            ->name('user.assignSchoolModalIndex');
        Route::get('/user/assignClassModal/{entryID?}', 'UserController@assignClassModalIndex')
            ->name('user.assignClassModalIndex');
        Route::get('/user/assignChildrenModal/{entryID?}', 'UserController@assignChildrenModalIndex')
            ->name('user.assignChildrenModalIndex');

        Route::post('/user/assignSchoolModal/{entryID}', 'UserController@assignSchoolModalStore')
            ->name('user.assignSchoolModalStore');
        Route::post('/user/assignClassModal/{entryID}', 'UserController@assignClassModalStore')
            ->name('user.assignClassModalStore');
        Route::post('/user/assignChildrenModal/{entryID}', 'UserController@assignChildrenModalStore')
            ->name('user.assignChildrenModalStore');

        Route::middleware(['role:super_admin'])->group(function () {

            Route::resource('/role', 'RoleController');
            Route::post('/role/search', 'RoleController@search')->name('role.search');

            // School Admin Routes
            Route::resource('/school_admin', 'SchoolAdminController');
            Route::post('/school_admin/search', 'SchoolAdminController@search')->name('school_admin.search');

            // Parent Routes
            Route::resource('/parent', 'ParentController');
            Route::post('/parent/search', 'ParentController@search')->name('parent.search');

            // Class Routes
            Route::resource('/class', 'ClassController');
            Route::post('/class/search', 'ClassController@search')->name('class.search');

        });

        // Teacher Routes
        Route::resource('/teacher', 'TeacherController');
        Route::post('/teacher/search', 'TeacherController@search')->name('teacher.search');

        Route::get('/school_admin/classes/{school_id?}', 'SchoolAdminController@classes')->name('school_admin.classes');
        Route::post('/school_admin/classes/{school_id?}', 'SchoolAdminController@classes_store')->name('school_admin.classesStore');

    });