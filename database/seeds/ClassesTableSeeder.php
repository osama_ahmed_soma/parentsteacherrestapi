<?php

use Illuminate\Database\Seeder;

class ClassesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $classes = [
            [
                'name' => '1'
            ],
            [
                'name' => '2'
            ],
            [
                'name' => '3'
            ]
        ];
        \App\Models\Classes::insert($classes);
    }
}
