<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $usersData = [
            [
                'name' => 'Teacher 1',
                'email' => 't@gmail.com',
                'phone_number' => '202-555-0123',
                'password' => bcrypt('temp'),
                'role' => 'teacher'
            ],
            [
                'name' => 'Parent 1',
                'email' => 'p@gmail.com',
                'phone_number' => '202-555-0124',
                'password' => bcrypt('temp'),
                'role' => 'parent'
            ],
            [
                'name' => 'School Admin',
                'email' => 'sa@gmail.com',
                'phone_number' => '202-555-0125',
                'password' => bcrypt('temp'),
                'role' => 'school_admin'
            ],
            [
                'name' => 'Admin',
                'email' => 'admin@admin.com',
                'phone_number' => '202-555-0126',
                'password' => bcrypt('temp'),
                'role' => 'super_admin'
            ]
        ];

        foreach ($usersData as $userData) {
            $user = new \App\Models\User();
            $user->name = $userData['name'];
            $user->email = $userData['email'];
            $user->phone_number = $userData['phone_number'];
            $user->password = $userData['password'];
            $user->save();
            $role = \App\Models\Role::where('name', $userData['role'])->first();
            $user->attachRole($role);
        }
    }
}
