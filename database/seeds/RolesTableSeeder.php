<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'name' => 'super_admin',
                'display_name' => 'Super Administrator',
                'description' => ''
            ],
            [
                'name' => 'school_admin',
                'display_name' => 'School Administrator',
                'description' => ''
            ],
            [
                'name' => 'parent',
                'display)_name' => 'Parent',
                'description' => ''
            ],
            [
                'name' => 'teacher',
                'display_name' => 'Teacher',
                'description' => ''
            ],
            [
                'name' => 'student_child',
                'display_name' => 'Student or Child',
                'description' => ''
            ],
        ];

        \App\Models\Role::insert($roles);
    }
}
