<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{

    private $imagePath = '/images/users/';

    private function getImage()
    {
        return asset($this->imagePath . $this->roles()->first()->name . DIRECTORY_SEPARATOR . $this->image);
    }

    public function toArray($request)
    {
        $userData = parent::toArray($request);
        if ($userData['image']) {
            $userData['image'] = $this->getImage();
        }
        $userData['role'] = $this->roles()->first();
        // check if role is teacher then get classes
        if ($this->hasRole('teacher') || $this->hasRole('student_child')) {
            $userData['class'] = $this->classTeacherStudent()->first();
        }
        if ($this->hasRole('student_child')) {
            $userData['parent'] = $this->parentStudent()->first();
            $userData['meta'] = $this->studentMeta()->first();
        }
        if ($this->hasRole('parent')) {

        }
        return $userData;
    }
}
