<?php

namespace App\Http\Controllers\Backend;

use App\Helper\UserHelper;
use App\Models\Classes;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{

    private $customData = [
        'title' => [
            'singular' => 'User',
            'plural' => 'Users'
        ],
        'routeName' => 'user'
    ];

    public function index(Request $request)
    {
        $users = UserHelper::getUsers();
        $roles = Role::all();
        $customData = $this->customData;
        if ($request->ajax()) {
            return view('admin.user.listing', compact('users', 'customData'));
        }
        return view('admin.user.index', compact('users', 'roles', 'customData'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = UserHelper::addUser($request);
        return (isset($user->id)) ? response()->json(['bool' => true, 'message' => 'User Added']) : response()->json(['bool' => false, 'errors' => ['Operation failed. Please try again.']], 422);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = UserHelper::updateUser($request, $id);
        return ($user) ?
            response()->json(['bool' => true, 'message' => 'User Updated']) :
            response()->json(['bool' => false, 'errors' => ['Operation failed. Please try again.']], 422);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if (isset($user->id)) {
            $user->delete();
            return response()->json(['bool' => true, 'message' => 'User Deleted']);
        }
        return response()->json(['bool' => false, 'errors' => ['Unable to delete user.']], 422);
    }

    public function search(Request $request)
    {
        $customData = $this->customData;
        $users = UserHelper::search($request);
        return (count($users) > 0) ? view('admin.user.listing', compact('users', 'customData')) : response()->json(['bool' => false, 'errors' => ['No User found']], 422);
    }

    public function assignSchoolModalIndex(Request $request, $entryID)
    {
        $schools = User::with('roles')->whereHas('roles', function ($query) {
            $query->where('name', 'school_admin');
        })->get();
        return view('admin.user.partial.assignSchoolModal', compact('schools', 'entryID'));
    }

    public function assignSchoolModalStore(Request $request, $entryID)
    {
        $user = User::find($entryID);
        $user->schools()->sync([$request->schoolID]);
        return response()->json(['bool' => true, 'message' => 'School Assigned with ' . ($user->roles()->first()->display_name) . ' Successfully.']);
    }

    public function assignClassModalIndex(Request $request, $entryID)
    {
        $classes = User::find($entryID)->schools()->first()->classes;
        return view('admin.user.partial.assignClassModal', compact('classes', 'entryID'));
    }

    public function assignClassModalStore(Request $request, $entryID)
    {
        $user = User::find($entryID);
        $user->classTeacherStudent()->sync([$request->classID]);
        return response()->json(['bool' => true, 'message' => 'Class Assigned with ' . ($user->roles()->first()->display_name) . ' Successfully.']);
    }

    public function assignChildrenModalIndex($entryID)
    {
//        $parent = User::find($entryID);
        // get all children's
        $students = User::whereHas('roles', function ($query) {
            $query->where('name', 'student_child');
        })->get();
        return view('admin.user.partial.assignChildrenModal', compact('students', 'entryID'));
    }

    public function assignChildrenModalStore(Request $request, $entryID)
    {
        $user = User::find($entryID);
        $user->studentParent()->sync([$request->studentID]);
        return response()->json(['bool' => true, 'message' => 'Student Assigned with ' . ($user->roles()->first()->display_name) . ' Successfully.']);
    }
}
