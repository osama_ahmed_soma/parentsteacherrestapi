<?php

namespace App\Http\Controllers\Backend;

use App\Helper\UserHelper;
use App\Models\Classes;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SchoolAdminController extends Controller
{

    private $customData = [
        'title' => [
            'singular' => 'School Admin',
            'plural' => 'School Admins'
        ],
        'routeName' => 'school_admin'
    ];

    public function index(Request $request)
    {
        $users = UserHelper::getUsers('school_admin');
        $roles = Role::all();
        $classes = Classes::all();
        $customData = $this->customData;
        if ($request->ajax()) {
            return view('admin.user.listing', compact('users', 'customData'));
        }

        return view('admin.user.index', compact('users', 'customData', 'roles', 'classes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = UserHelper::addUser($request);
        return (isset($user->id)) ? response()->json(['bool' => true, 'message' => 'School Admin Added']) : response()->json(['bool' => false, 'errors' => ['Operation failed. Please try again.']], 422);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = UserHelper::updateUser($request, $id);
        return ($user) ?
            response()->json(['bool' => true, 'message' => 'School Admin Updated']) :
            response()->json(['bool' => false, 'errors' => ['Operation failed. Please try again.']], 422);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if (isset($user->id)) {
            $user->delete();
            return response()->json(['bool' => true, 'message' => 'School Admin Deleted']);
        }
        return response()->json(['bool' => false, 'errors' => ['Unable to delete school admin.']], 422);
    }

    public function search(Request $request)
    {
        $customData = $this->customData;
        $users = UserHelper::search($request, 'school_admin');
        return (count($users) > 0) ? view('admin.user.listing', compact('users', 'customData')) : response()->json(['bool' => false, 'errors' => ['No School Admin found']], 422);
    }

    public function classes($id)
    {
        $userClasses = User::find($id)->classes;
        $userClassesID = [];
        foreach ($userClasses as $class) {
            $userClassesID[] = $class->id;
        }
        $classes = Classes::all();
        $schoolID = $id;
        return view('admin.user.classes', compact('classes', 'userClassesID', 'schoolID'));
    }

    public function classes_store(Request $request, $id)
    {
        $request->validate([
            'classesIDs' => 'required|array|min:1'
        ]);
        $schoolAdmin = User::find($id);
        $schoolAdmin->classes()->sync($request->classesIDs);
        return response()->json(['bool' => true, 'message' => 'Classes Updated']);
    }
}
