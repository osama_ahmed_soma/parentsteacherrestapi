<?php

namespace App\Http\Controllers\Backend;

use App\Helper\UserHelper;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TeacherController extends Controller
{

    private $customData = [
        'title' => [
            'singular' => 'Teacher',
            'plural' => 'Teachers'
        ],
        'routeName' => 'teacher'
    ];

    public function index(Request $request)
    {
        $users = UserHelper::getUsers('teacher');
        $roles = Role::all();
        $customData = $this->customData;
        $schools = User::with('roles')->whereHas('roles', function ($query) {
            $query->where('name', 'school_admin');
        })->get();
        if ($request->ajax()) {
            return view('admin.user.listing', compact('users', 'customData'));
        }

        return view('admin.user.index', compact('users', 'roles', 'customData', 'schools'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = UserHelper::addUser($request);
        return (isset($user->id)) ? response()->json(['bool' => true, 'message' => 'Teacher Added']) : response()->json(['bool' => false, 'errors' => ['Operation failed. Please try again.']], 422);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = UserHelper::updateUser($request, $id);
        return ($user) ?
            response()->json(['bool' => true, 'message' => 'Teacher Updated']) :
            response()->json(['bool' => false, 'errors' => ['Operation failed. Please try again.']], 422);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if (isset($user->id)) {
            $user->delete();
            return response()->json(['bool' => true, 'message' => 'Teacher Deleted']);
        }
        return response()->json(['bool' => false, 'errors' => ['Unable to delete teacher.']], 422);
    }

    public function search(Request $request)
    {
        $customData = $this->customData;
        $users = UserHelper::search($request, 'teacher');
        return (count($users) > 0) ? view('admin.user.listing', compact('users', 'customData')) : response()->json(['bool' => false, 'errors' => ['No Teacher found']], 422);
    }
}
