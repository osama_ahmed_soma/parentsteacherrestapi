<?php

namespace App\Http\Controllers\Backend;

use App\Models\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $roles = Role::orderBy('id', 'desc')->paginate(15);

        if ($request->ajax()) {
            return view('admin.role.listing', compact('roles'));
        }

        return view('admin.role.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:roles'
        ]);
        $role = Role::create($request->all());
        return (isset($role->id)) ? response()->json(['bool' => true, 'message' => 'Role Added']) : response()->json(['bool' => false, 'errors' => ['Operation failed. Please try again.']], 422);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|unique:roles,name,' . $id
        ]);
        $role = Role::where('id', $id)->update($request->except(['_method', '_token', 'id']));
        return ($role) ?
            response()->json(['bool' => true, 'message' => 'Role Updated']) :
            response()->json(['bool' => false, 'errors' => ['Operation failed. Please try again.']], 422);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::find($id);
        if (isset($role->id)) {
            $role->delete();
            return response()->json(['bool' => true, 'message' => 'Role Deleted']);
        }
        return response()->json(['bool' => false, 'errors' => ['Unable to delete role.']], 422);
    }


    public function search(Request $request)
    {
        $request->validate([
            'search' => 'required'
        ]);
        $roles = Role::search($request->search)->paginate(15);
        return ($roles) ? view('admin.role.listing', compact('roles')) : response()->json(['bool' => false, 'errors' => ['No Role found']], 422);
    }

}
