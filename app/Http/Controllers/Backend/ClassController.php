<?php

namespace App\Http\Controllers\Backend;

use App\Models\Classes;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClassController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $classes = Classes::orderBy('id', 'desc')->paginate(15);
        if ($request->ajax()) {
            return view('admin.class.listing', compact('classes'));
        }
        return view('admin.class.index', compact('classes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:classes'
        ]);
        $class = Classes::create($request->all());
        return (isset($class->id)) ? response()->json(['bool' => true, 'message' => 'Class Added']) : response()->json(['bool' => false, 'errors' => ['Operation failed. Please try again.']], 422);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|unique:classes,name,' . $id
        ]);
        $class = Classes::where('id', $id)->update($request->except(['_method', '_token', 'id']));
        return ($class) ?
            response()->json(['bool' => true, 'message' => 'Class Updated']) :
            response()->json(['bool' => false, 'errors' => ['Operation failed. Please try again.']], 422);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $class = Classes::find($id);
        if (isset($class->id)) {
            $class->delete();
            return response()->json(['bool' => true, 'message' => 'Class Deleted']);
        }
        return response()->json(['bool' => false, 'errors' => ['Unable to delete class.']], 422);
    }

    public function search(Request $request)
    {
        $request->validate([
            'search' => 'required'
        ]);
        $classes = Classes::search($request->search)->paginate(15);
        return ($classes) ? view('admin.class.listing', compact('classes')) : response()->json(['bool' => false, 'errors' => ['No Class found']], 422);
    }
}
