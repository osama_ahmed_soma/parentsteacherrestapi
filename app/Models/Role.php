<?php

namespace App\Models;

use Zizaco\Entrust\EntrustRole;
use Nicolaslopezj\Searchable\SearchableTrait;

class Role extends EntrustRole
{
    use SearchableTrait;
    protected $fillable = [
        'name',
        'display_name',
        'description'
    ];
    protected $searchable = [
        'columns' => [
            'roles.name' => 10,
            'roles.display_name' => 9,
            'roles.description' => 8,
        ]
    ];
}
