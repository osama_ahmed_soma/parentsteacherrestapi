<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Classes extends Model
{
    use SearchableTrait;
    protected $fillable = [
        'name'
    ];
    protected $searchable = [
        'columns' => [
            'classes.name' => 10,
        ]
    ];

    public function teachersStudents()
    {
        return $this->belongsToMany(User::class, 'class_teachers_students', 'class_id', 'teacher_student_id');
    }
}
