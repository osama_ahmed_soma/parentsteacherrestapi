<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Nicolaslopezj\Searchable\SearchableTrait;

class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait;
    use SearchableTrait;
    use HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'phone_number', 'password', 'image'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $searchable = [
        'columns' => [
            'users.name' => 10,
            'users.phone_number' => 9,
            'users.email' => 8,
            'roles.name' => 7,
            'roles.display_name' => 6
        ],
        'joins' => [
            'role_user' => ['users.id', 'role_user.user_id'],
            'roles' => ['role_user.role_id', 'roles.id'],
        ]
    ];

    public function findForPassport($username)
    {
        return $this->where('phone_number', $username)->whereHas('roles', function ($query) {
            $query->whereIn('name', [
                'teacher',
                'parent'
            ]);
        })->first();
    }

    public function classes()
    {
        return $this->belongsToMany(Classes::class, 'school_class', 'school_id', 'class_id');
    }

    public function schools()
    {
        return $this->belongsToMany(User::class, 'school_teachers_students', 'teacher_student_id', 'school_id');
    }

    public function classTeacherStudent()
    {
        return $this->belongsToMany(Classes::class, 'class_teachers_students', 'teacher_student_id', 'class_id');
    }

    public function studentMeta()
    {
        return $this->hasMany(StudentMeta::class, 'student_id');
    }

    public function studentParent()
    {
        return $this->belongsToMany(User::class, 'student_parent', 'parent_id', 'student_id');
    }

    public function parentStudent()
    {
        return $this->belongsToMany(User::class, 'student_parent', 'student_id', 'parent_id');
    }

}
