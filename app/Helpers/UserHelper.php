<?php

namespace App\Helper;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;

class UserHelper
{
    private static $imagePath = '/images/users/';

    private static function getImagePath(Role $role)
    {
        return self::$imagePath . DIRECTORY_SEPARATOR . $role->name;
    }

    private static function getValidatedImage(Role $role, $imageName)
    {
        $image = public_path(self::getImagePath($role) . DIRECTORY_SEPARATOR . $imageName);
        if (file_exists($image)) {
            return $image;
        }
        return false;
    }

    private static function uploadImage(Role $role, $image = null)
    {
        $input = [];
        $input['imageName'] = null;
        if (!empty($image)) {
            $input['imageName'] = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path(self::getImagePath($role));
            $image->move($destinationPath, $input['imageName']);
        }
        return $input['imageName'];
    }

    public static function getUsers($type = '*')
    {
        switch ($type) {
            case '*':
                $users = User::with('roles')->orderBy('id', 'desc')->paginate(15);
                break;
            case 'teacher':
                $users = User::with(['roles', 'schools'])->whereHas('roles', function ($query) use ($type) {
                    $query->where('name', $type);
                })->orderBy('id', 'desc')->paginate(15);
                break;
            case 'school_admin':
                $users = User::with(['roles', 'classes'])->whereHas('roles', function ($query) use ($type) {
                    $query->where('name', $type);
                })->orderBy('id', 'desc')->paginate(15);
                break;
            case 'parent':
                $users = User::with('roles')->whereHas('roles', function ($query) use ($type) {
                    $query->where('name', $type);
                })->orderBy('id', 'desc')->paginate(15);
                break;
            default:
                $users = User::with('roles')->orderBy('id', 'desc')->paginate(15);
        }
        return $users;
    }

    public static function addUser(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'nullable|unique:users',
            'phone_number' => 'required|phone:AUTO,PK|unique:users',
            'roles' => 'required|exists:roles,name',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $requestData = $request->all();
        $role = Role::where('name', $requestData['roles'])->first();
        $requestData['image'] = self::uploadImage($role, $request->file('image'));
        $requestData['password'] = ($requestData['password']) ? bcrypt($requestData['password']) : bcrypt('temp');
        $user = User::create($requestData);
        $user->roles()->sync($role->id);
        return $user;
    }

    public static function updateUser(Request $request = null, $id = null)
    {
        if (!$id) {
            return false;
        }
        $request->validate([
            'name' => 'required',
            'email' => 'nullable|unique:users,email,' . $id,
            'phone_number' => 'required|phone:AUTO,PK|unique:users,phone_number,' . $id,
            'roles' => 'required|exists:roles,name',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $requestData = $request->except(['_method', '_token', 'id']);
        $role = Role::where('name', $requestData['roles'])->first();
        $requestData['image'] = self::uploadImage($role, $request->file('image'));
        $requestData['password'] = ($requestData['password']) ? bcrypt($requestData['password']) : bcrypt('temp');

        $previousImageName = User::where('id', $id)->first()->image;
        $image = self::getValidatedImage($role, $previousImageName);
        if ($previousImageName && $image) {
            unlink($image);
        }

        $user = User::where('id', $id)->update($requestData);
        $user->roles()->sync($role->id);
        return ($user) ? $user : false;
    }

    public static function deleteUser($id)
    {
        $user = User::find($id);
        if (isset($user->id)) {
            $user->delete();
            return true;
        }
        return false;
    }

    public static function search(Request $request, $type = '*')
    {
        $request->validate([
            'search' => 'required'
        ]);
        if ($type === '*') {
            $users = User::search($request->search)->with('roles')->paginate(15);
        } else {
            $users = User::search($request->search)->with('roles')->whereHas('roles', function ($query) use ($type) {
                $query->where('name', $type);
            })->paginate(15);
        }
        return $users;
    }

}