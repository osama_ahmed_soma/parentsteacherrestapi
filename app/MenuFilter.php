<?php

namespace App;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use JeroenNoten\LaravelAdminLte\Menu\Builder;
use JeroenNoten\LaravelAdminLte\Menu\Filters\FilterInterface;

class MenuFilter implements FilterInterface
{
    public function transform($item, Builder $builder)
    {
        $user = Auth::user();
        $user = User::find($user->id);
        if (isset($item['has-role']) && !$user->hasRole($item['has-role'])) {
            return false;
        }

        return $item;
    }
}