<div class="modal fade" id="create_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{route('admin.class.store')}}" method="post" class="create-form">
                @include('admin.class.partial.form', ['label' => 'Create'])
            </form>
        </div>
    </div>
</div>