<table id="example" class="table table-bordered table-hover tablesorter customTableSort">
    <thead>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Created At</th>
        <th>Action(s)</th>
    </tr>
    </thead>
    <tbody>
    @if (!empty($classes))
        @foreach ( $classes as $class )
            <tr id="class_{{$class->id}}">
                <td>{{$class->id}}</td>
                <td>{{$class->name}}</td>
                <td class="created_at">{{($class->created_at) ? $class->created_at->diffForHumans() : 'N/A'}}</td>
                <td>
                    <div class="btn-group">
                        <a class="btn btn-sm btn-primary edit_entry modalOpens" title="Edit" href="#"
                           data-props="{{$class}}"
                           data-toggle="modal" data-target="#edit_modal">
                            <i class="fa fa-edit"></i>
                        </a>
                        <a class="delete btn btn-sm btn-danger entry_remove"
                           data-route="{{route('admin.class.index')}}" data-entry_id="{{$class->id}}"
                           title="Delete">
                            <i class="fa fa-remove"></i>
                        </a>
                    </div>
                </td>
            </tr>
        @endforeach
    @else
        <tr>
            <td colspan="4">There is no Class.</td>
        </tr>
    @endif
    </tbody>
    <tfoot>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Created At</th>
        <th>Action(s)</th>
    </tr>
    </tfoot>
</table>

{{ $classes->links() }}