<div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{route('admin.class.index')}}" method="post" class="edit-form">
                {{method_field('PUT')}}
                @include('admin.class.partial.form', ['label' => 'Edit'])
                <input type="hidden" name="id" id="id" value=""/>
            </form>
        </div>
    </div>
</div>