<script src="{{asset('/vendor/adminlte/vendor/noty/lib/noty.min.js')}}"></script>
<script src="{{asset('/vendor/adminlte/vendor/__jquery.tablesorter/jquery.tablesorter.min.js')}}"></script>
<script>
    var globalData = function () {
        this.defaultImagePath = '{{asset('/images')}}';
        this.defaultImage = this.defaultImagePath + '/userDefaultIcon.png';
    };
</script>
<script src="{{asset('/js/custom.js')}}"></script>
<div class="customLoader">
    <img src="{{asset('/images/spinner.gif')}}" alt="Loading...">
</div>