<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">{{$label}} {{$customData['title']['singular']}}</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" name="name" id="name"
                       placeholder="{{$customData['title']['singular']}} Name"/>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" name="email" id="email"
                       placeholder="{{$customData['title']['singular']}} Email"/>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label for="phone_number">Phone Number</label>
                <input type="text" class="form-control" name="phone_number" id="phone_number"
                       placeholder="{{$customData['title']['singular']}} Phone Number"/>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control" name="password" id="password"
                       placeholder="{{$customData['title']['singular']}} Password"/>
                <span class="help-block">Leave blank for default password (temp).</span>
            </div>
        </div>
        <div class="col-md-12">
            <div class="row">
                <div class="col-sm-9">
                    <div class="form-group">
                        <label for="image">Image</label>
                        <input type="file" class="form-control" name="image" id="image"/>
                    </div>
                </div>
                <div class="col-sm-3 hidden-xs text-right">
                    <img src="{{asset('/images/userDefaultIcon.png')}}" image class="avatar-form">
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label for="roles">Roles</label>
                <select name="roles" id="roles" class="form-control">
                    @if (!empty($roles))
                        @foreach ( $roles as $role )
                            <option value="{{$role->name}}">{{($role->display_name) ? $role->display_name : $role->name}}</option>
                        @endforeach
                    @else
                        <option value="">No Role exists.</option>
                    @endif
                </select>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    {{ csrf_field() }}
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="submit" class="btn btn-primary action-btn">{{$label}}</button>
</div>