<div class="box-header with-border searchFormHeader">
    <h3 class="box-title">Search Area</h3>
</div>
<form action="{{route('admin.'.$customData['routeName'].'.search')}}" method="post" class="searchForm">
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="search">Search</label>
                    <input type="text" class="form-control" id="search" name="search" placeholder="Search by any field">
                </div>
            </div>
        </div>
        {{--<div class="form-group">--}}
        {{--<label for="exampleInputPassword1">Password</label>--}}
        {{--<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">--}}
        {{--</div>--}}
        {{--<div class="form-group">--}}
        {{--<label for="exampleInputFile">File input</label>--}}
        {{--<input type="file" id="exampleInputFile">--}}

        {{--<p class="help-block">Example block-level help text here.</p>--}}
        {{--</div>--}}
        {{--<div class="checkbox">--}}
        {{--<label>--}}
        {{--<input type="checkbox"> Check me out--}}
        {{--</label>--}}
        {{--</div>--}}
    </div>
    <!-- /.box-body -->

    <div class="box-footer">
        {{ csrf_field() }}
        <button type="button" class="btn btn-default"
                onclick="(function(){ startLoading(); renderHTML(null, function(){ stopLoading(); }); }())">Reset
        </button>
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</form>