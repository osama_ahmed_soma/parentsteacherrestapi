<div class="modal fade" id="assignClassModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{route('admin.user.assignClassModalStore', [
                'entryID' => $entryID
            ])}}" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Assign Class</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="classID">Classes</label>
                                <select name="classID" id="classID" class="form-control">
                                    @if (!empty($classes))
                                        @foreach ( $classes as $index => $class )
                                            <option value="{{$class->id}}" {{$index === 0 ? 'selected' : ''}}>{{($class->name)}}</option>
                                        @endforeach
                                    @else
                                        <option value="">No Class exists.</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {{ csrf_field() }}
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary action-btn">Assign</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#assignClassModal form').submit(function (e) {
            e.preventDefault();
            ajax_request($(this).attr('action'), $(this), 'POST');
            return false;
        });
    });
</script>