<div class="modal fade" id="assignChildrenModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{route('admin.user.assignChildrenModalStore', [
                'entryID' => $entryID
            ])}}" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Assign Children</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="studentID">Student</label>
                                <select name="studentID" id="studentID" class="form-control">
                                    @if (!empty($students))
                                        @foreach ( $students as $index => $student )
                                            <option value="{{$student->id}}" {{$index === 0 ? 'selected' : ''}}>{{($student->name)}}</option>
                                        @endforeach
                                    @else
                                        <option value="">No Student exists.</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {{ csrf_field() }}
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary action-btn">Assign</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#assignChildrenModal form').submit(function (e) {
            e.preventDefault();
            ajax_request($(this).attr('action'), $(this), 'POST');
            return false;
        });
    });
</script>