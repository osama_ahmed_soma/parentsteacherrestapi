@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>{{$customData['title']['plural']}} List
        <button class="btn btn-primary modalOpens" data-toggle="modal" data-target="#create_modal">Create
            {{$customData['title']['singular']}}
        </button>
    </h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">{{$customData['title']['plural']}}</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                @include('admin.user.partial.searchForm')
            </div>
        </div>
        <div class="col-md-12">
            <div class="box">
                <div class="box-body" id="ajax-container">
                    @include('admin.user.listing')
                </div>
            </div>
        </div>
    </div>
    @include('admin.user.create')
    @include('admin.user.edit')
    <div id="ajaxModals"></div>
@stop

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('admin.css')
@stop

@section('js')
    @include('admin.javascript')
    <script>
        var errorHandler = function (data) {
            stopLoading();
            var result = JSON.parse(data.responseText);
            $.each(result.errors, function (k, v) {
                new Noty({
                    type: 'error',
                    layout: 'topRight',
                    text: v
                }).show();
            });
        };
        var getClasses = function () {
            var value = $('#teacher_school').val();
            startLoading();
            $.ajax({
                url: '{{route('admin.school_admin.classes')}}/' + value,
                data: {},
                method: 'GET',
                success: function (data) {
                    $('#teacher_class').html(data);
                    stopLoading();
                },
                error: function (data) {
                    stopLoading();
                    var result = JSON.parse(data.responseText);
                    $.each(result.errors, function (k, v) {
                        new Noty({
                            type: 'error',
                            layout: 'topRight',
                            text: v
                        }).show();
                    });
                }
            });

        };
        $(document).ready(function () {
            $('.create-form').submit(function (e) {
                e.preventDefault();

                var $url = $(this).attr('action'),
                    $self = $(this);

                ajax_request($url, $self, 'POST');

                return false;
            });
            $('.edit-form').submit(function (e) {
                e.preventDefault();

                var $url = $(this).attr('action') + '/' + $(this).find('input[name="id"]').val(),
                    $self = $(this);

                ajax_request($url, $self, 'PUT');

                return false;
            });
            $(document).on('click', '.edit_entry', function (e) {
                var attributes = $(this).data();
                attributes = attributes.props;
                $.each(attributes, function (k, v) {
                    if (k === 'classes') {
                        if (v.length > 0) {
                            v = v[0];
                        }
                        $('.edit-form #school_class').val(v.id);
                    }
                });
            });
            $(document).on('click', '.assignSchoolBtn', function (e) {
                e.preventDefault();
                var entryID = $(this).data().entry_id;
                $.ajax({
                    url: '{{route('admin.user.assignSchoolModalIndex')}}/' + entryID,
                    method: 'GET',
                    data: {},
                    success: function (data) {
                        $('#ajaxModals').html(data);
                        $('#assignSchoolModal').modal('show');
                        stopLoading();
                    },
                    error: errorHandler
                });
                return false;
            });
            $(document).on('click', '.assignClassBtn', function (e) {
                e.preventDefault();
                var entryID = $(this).data().entry_id;
                $.ajax({
                    url: '{{route('admin.user.assignClassModalIndex')}}/' + entryID,
                    method: 'GET',
                    data: {},
                    success: function (data) {
                        $('#ajaxModals').html(data);
                        $('#assignClassModal').modal('show');
                        stopLoading();
                    },
                    error: errorHandler
                });
                return false;
            });
            $(document).on('click', '.assignChildrenBtn', function (e) {
                e.preventDefault();
                var entryID = $(this).data().entry_id;
                $.ajax({
                    url: '{{route('admin.user.assignChildrenModalIndex')}}/' + entryID,
                    method: 'GET',
                    data: {},
                    success: function (data) {
                        $('#ajaxModals').html(data);
                        $('#assignChildrenModal').modal('show');
                        stopLoading();
                    },
                    error: errorHandler
                });
                return false;
            });
        });
    </script>
@stop