<div class="table-responsive">
    <table id="example" class="table table-bordered table-hover tablesorter customTableSort">
        <thead>
        <tr>
            <th>ID</th>
            <th>Image</th>
            <th>Name</th>
            <th>Email</th>
            <th>Phone Number</th>
            <th>Role</th>
            @if($customData['routeName'] === 'teacher')
                <th>School</th>
            @endif
            <th>Created At</th>
            <th>Action(s)</th>
        </tr>
        </thead>
        <tbody>
        @if (!empty($users))
            @foreach ( $users as $user )
                <tr id="user_{{$user->id}}">
                    <td>{{$user->id}}</td>
                    <td>
                        <img src="{{($user->image) ? asset('/images/users/'.$user->roles[0]->name.'/'.$user->image) : asset('/images/userDefaultIcon.png')}}"
                             class="avatar">
                    </td>
                    <td>{{$user->name}}</td>
                    <td>{{($user->email) ? $user->email : 'N/A'}}</td>
                    <td>{{$user->phone_number}}</td>
                    <td>{{($user->roles[0]->display_name) ? $user->roles[0]->display_name : $user->roles[0]->name}}</td>
                    @if($customData['routeName'] === 'teacher')
                        <td>{{(!empty($user->schools) && count($user->schools) > 0) ? $user->schools[0]->name : 'N/A'}}</td>
                    @endif
                    <td class="created_at">{{($user->created_at) ? $user->created_at->diffForHumans() : 'N/A'}}</td>
                    <td>
                        <div class="btn-group">
                            @if($user->roles[0]->name === 'school_admin')
                                <a class="btn btn-sm btn-primary" href="{{route('admin.school_admin.classes', [
                                    'school_id' => $user->id
                                ])}}">
                                    Assign Classes
                                </a>
                            @elseif($user->roles[0]->name === 'teacher' || $user->roles[0]->name === 'student_child')
                                @if(Auth::user()->hasRole('super_admin'))
                                    <a href="#" class="btn btn-sm btn-primary assignSchoolBtn"
                                       data-entry_id="{{$user->id}}">
                                        Assign School
                                    </a>
                                    @if(!empty($user->schools && count($user->schools) > 0))
                                        <a href="#" class="btn btn-sm btn-primary assignClassBtn"
                                           data-entry_id="{{$user->id}}">
                                            Assign Class
                                        </a>
                                    @endif
                                @else
                                    <a href="#" class="btn btn-sm btn-primary assignClassBtn"
                                       data-entry_id="{{$user->id}}">
                                        Assign Class
                                    </a>
                                @endif
                            @elseif($user->roles[0]->name === 'parent')
                                <a href="#" class="btn btn-sm btn-primary assignChildrenBtn"
                                   data-entry_id="{{$user->id}}">
                                    Assign Children
                                </a>
                            @endif
                            <a class="btn btn-sm btn-primary edit_entry modalOpens" title="Edit" href="#"
                               data-props="{{$user}}"
                               data-toggle="modal" data-target="#edit_modal">
                                <i class="fa fa-edit"></i>
                            </a>
                            <a class="delete btn btn-sm btn-danger entry_remove"
                               data-route="{{route('admin.'.$customData['routeName'].'.index')}}"
                               data-entry_id="{{$user->id}}"
                               title="Delete">
                                <i class="fa fa-remove"></i>
                            </a>
                        </div>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="4">There is no {{$customData['title']['singular']}}.</td>
            </tr>
        @endif
        </tbody>
        <tfoot>
        <tr>
            <th>ID</th>
            <th>Image</th>
            <th>Name</th>
            <th>Email</th>
            <th>Phone Number</th>
            <th>Role</th>
            @if($customData['routeName'] === 'teacher')
                <th>School</th>
            @endif
            <th>Created At</th>
            <th>Action(s)</th>
        </tr>
        </tfoot>
    </table>
</div>

{{ $users->links() }}