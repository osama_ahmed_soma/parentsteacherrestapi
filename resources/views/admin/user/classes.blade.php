@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Classes List</h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Classes</li>
    </ol>
@stop

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body" id="ajax-container">
                    <form id="classesForm" action="{{route('admin.school_admin.classesStore', [
                        'school_id' => $schoolID
                    ])}}" method="post" onsubmit="return false;">
                        <div style="overflow: hidden;">
                            @if(!empty($classes))
                                @foreach ( $classes as $class )
                                    <div class="col-sm-2 col-xs-3">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox"
                                                       value="{{$class->id}}"
                                                       name="classesIDs[]" {{(in_array($class->id, $userClassesID)) ? 'checked' : ''}}> {{$class->name}}
                                            </label>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                No Class found.
                            @endif
                        </div>
                        <div>
                            <div class="text-right">
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@stop

@section('css')
    @include('admin.css')
@stop
@section('js')
    @include('admin.javascript')
    <script>
        $(document).ready(function () {
            $(document).on('submit', 'form#classesForm', function (e) {
                e.preventDefault();
                var $this = this;
                // ajax_request($($this).attr('action'), $($this), 'POST');
                startLoading();
                var form = $($this)[0];
                var data = new FormData(form);
                $.ajax({
                    url: $($this).attr('action'),
                    data: data,
                    enctype: 'multipart/form-data',
                    method: 'POST',
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function (data) {
                        var result = data;
                        new Noty({
                            type: 'success',
                            layout: 'topRight',
                            text: result.message
                        }).show();

                        stopLoading();

                    },

                    error: function (data) {

                        stopLoading();

                        var result = JSON.parse(data.responseText);

                        $.each(result.errors, function (k, v) {
                            new Noty({
                                type: 'error',
                                layout: 'topRight',
                                text: v
                            }).show();
                        });
                    }

                });
                return false;
            });
        });
    </script>
@stop