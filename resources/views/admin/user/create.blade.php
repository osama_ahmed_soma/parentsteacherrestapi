<div class="modal fade" id="create_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{route('admin.'.$customData['routeName'].'.store')}}" method="post" class="create-form">
                @include('admin.user.partial.form', ['label' => 'Create'])
            </form>
        </div>
    </div>
</div>