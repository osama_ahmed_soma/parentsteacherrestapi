<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">{{$label}} Role</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" name="name" id="name"
                       placeholder="Role Name"/>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label for="display_name">Display Name</label>
                <input type="text" class="form-control" name="display_name" id="display_name"
                       placeholder="Role Display Name"/>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label for="description">Description</label>
                <textarea name="description" id="description" class="form-control"
                          placeholder="Role Description"></textarea>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    {{ csrf_field() }}
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="submit" class="btn btn-primary action-btn">{{$label}}</button>
</div>