<table id="example" class="table table-bordered table-hover tablesorter customTableSort">
    <thead>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Display Name</th>
        <th>Description</th>
        <th>Created At</th>
        <th>Action(s)</th>
    </tr>
    </thead>
    <tbody>
    @if (!empty($roles))
        @foreach ( $roles as $role )
            <tr id="role_{{$role->id}}">
                <td>{{$role->id}}</td>
                <td>{{$role->name}}</td>
                <td>{{$role->display_name}}</td>
                <td>{{($role->description) ? $role->description : 'N/A'}}</td>
                <td class="created_at">{{($role->created_at) ? $role->created_at->diffForHumans() : 'N/A'}}</td>
                <td>
                    <div class="btn-group">
                        <a class="btn btn-sm btn-primary edit_entry modalOpens" title="Edit" href="#"
                           data-props="{{$role}}"
                           data-toggle="modal" data-target="#edit_modal">
                            <i class="fa fa-edit"></i>
                        </a>
                        <a class="delete btn btn-sm btn-danger entry_remove"
                           data-route="{{route('admin.role.index')}}" data-entry_id="{{$role->id}}"
                           title="Delete">
                            <i class="fa fa-remove"></i>
                        </a>
                    </div>
                </td>
            </tr>
        @endforeach
    @else
        <tr>
            <td colspan="4">There is no Role.</td>
        </tr>
    @endif
    </tbody>
    <tfoot>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Display Name</th>
        <th>Description</th>
        <th>Created At</th>
        <th>Action(s)</th>
    </tr>
    </tfoot>
</table>

{{ $roles->links() }}