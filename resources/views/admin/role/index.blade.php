@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Roles List
        <button class="btn btn-primary modalOpens" data-toggle="modal" data-target="#create_modal">Create
            Role
        </button>
    </h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Roles</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                @include('admin.role.partial.searchForm')
            </div>
        </div>
        <div class="col-md-12">
            <div class="box">
                <div class="box-body" id="ajax-container">
                    @include('admin.role.listing')
                </div>
            </div>
        </div>
    </div>
    @include('admin.role.create')
    @include('admin.role.edit')
@stop

@section('css')
    @include('admin.css')
@stop

@section('js')
    @include('admin.javascript')
    <script>
        function ajax_request($url, $self, $method) {
            startLoading();
            $.ajax({
                url: $url,
                data: $self.serialize(),
                method: $method,
                success: function (data) {
                    var result = data;
                    $self.find('input[name="name"], input[name="description"], input[name="sort_order"]').val('');
                    $self.parents('div.modal').modal('hide');

                    new Noty({
                        type: 'success',
                        layout: 'topRight',
                        text: result.message
                    }).show();

                    renderHTML();

                    stopLoading();

                },

                error: function (data) {

                    stopLoading();

                    var result = JSON.parse(data.responseText);

                    $.each(result.errors, function (k, v) {
                        new Noty({
                            type: 'error',
                            layout: 'topRight',
                            text: v
                        }).show();
                    });
                }

            });
        }

        $(document).ready(function () {
            $('.create-form').submit(function (e) {
                e.preventDefault();

                var $url = $(this).attr('action'),
                    $self = $(this);

                ajax_request($url, $self, 'POST');

                return false;
            });
            $('.edit-form').submit(function (e) {
                e.preventDefault();

                var $url = $(this).attr('action') + '/' + $(this).find('input[name="id"]').val(),
                    $self = $(this);

                ajax_request($url, $self, 'PUT');

                return false;
            });

        });
    </script>
@stop