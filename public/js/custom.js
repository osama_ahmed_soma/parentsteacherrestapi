var startLoading = function () {
    $('.customLoader').css('display', 'block');
};
var stopLoading = function () {
    $('.customLoader').css('display', 'none');
};
var renderHTML = function (url, cb, ele) {
    url = url || window.location.href;
    ele = ele || '#ajax-container';
    $(ele).load(url, function () {
        if (ele === '#ajax-container') {
            $(".customTableSort").tablesorter();
        }
        if (cb) {
            cb();
        }
    });
};
var readURL = function (input, cb) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = cb;
        reader.readAsDataURL(input.files[0]);
    }
};
var ajax_request = function ($url, $self, $method) {
    startLoading();
    var form = $self[0];
    var data = new FormData(form);
    $.ajax({
        url: $url,
        data: data,
        enctype: 'multipart/form-data',
        method: $method,
        processData: false,
        contentType: false,
        cache: false,
        success: function (data) {
            var result = data;
            $self.find('input[name="name"], input[name="description"], input[name="sort_order"]').val('');
            $self.parents('div.modal').modal('hide');
            renderHTML();

            new Noty({
                type: 'success',
                layout: 'topRight',
                text: result.message
            }).show();

            stopLoading();

        },

        error: function (data) {

            stopLoading();

            var result = JSON.parse(data.responseText);

            $.each(result.errors, function (k, v) {
                new Noty({
                    type: 'error',
                    layout: 'topRight',
                    text: v
                }).show();
            });
        }

    });
};
(function ($, globalData) {
    Noty.overrideDefaults({
        theme: 'relax',
        timeout: 5000
    });
    $(".customTableSort").tablesorter();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(document).on('click', '.modalOpens', function (e) {
        $('[image]').attr('src', globalData.defaultImage);
    });
    $(document).on('click', '.edit_entry', function (e) {
        var attributes = $(this).data();
        attributes = attributes.props;
        $.each(attributes, function (k, v) {
            if ($('.edit-form #' + k).length) {
                if (k === 'image') {
                    if (v) {
                        $('[' + k + ']').attr('src', globalData.defaultImagePath + '/' + v);
                    } else {
                        $('[' + k + ']').attr('src', globalData.defaultImage);
                    }
                } else {
                    if (k === 'roles' && typeof v === 'object') {
                        v = v[0].name;
                    }
                    $('.edit-form #' + k).val(v);
                }
            }
        });
    });
    $(document).on('click', '.entry_remove', function (e) {
        e.preventDefault();
        var $entry_id = $(this).data('entry_id'),
            $url = $(this).data('route') + '/' + $entry_id;

        var n = new Noty({
            text: 'Are you sure you want to delete this item?',
            buttons: [
                Noty.button('YES', 'btn btn-success', function () {
                    n.close();
                    startLoading();
                    $.ajax({
                        url: $url,
                        data: {id: $entry_id},
                        method: 'DELETE',
                        success: function (data) {
                            new Noty({
                                type: 'success',
                                layout: 'topRight',
                                text: data.message
                            }).show();
                            renderHTML();
                            stopLoading();
                        },
                        error: function (data) {
                            stopLoading();
                            var result = JSON.parse(data.responseText);
                            $.each(result.errors, function (k, v) {
                                new Noty({
                                    type: 'error',
                                    layout: 'topRight',
                                    text: v
                                }).show();
                            });
                        }
                    });
                    return false;
                }, {
                    id: 'button1',
                    'data-status': 'ok'
                }), Noty.button('NO', 'btn btn-error', function () {
                    n.close();
                })
            ]
        }).show();

    });

    $(document).on('click', '.searchFormHeader', function (e) {
        e.preventDefault();
        var searchForm = $('.searchForm');
        if (searchForm.is(':visible')) {
            searchForm.hide('slow');
        } else {
            searchForm.show('slow');
        }
    });
    $(document).on('submit', '.searchForm', function (e) {
        e.preventDefault();
        var searchForm = $(this);
        var data = new FormData(searchForm[0]);
        startLoading();
        $.ajax({
            url: searchForm.attr('action'),
            data: data,
            method: searchForm.attr('method'),
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                $('#ajax-container').html(data);
                $(".customTableSort").tablesorter();
                stopLoading();
            },
            error: function (data) {
                stopLoading();
                var result = JSON.parse(data.responseText);
                $.each(result.errors, function (k, v) {
                    new Noty({
                        type: 'error',
                        layout: 'topRight',
                        text: v
                    }).show();
                });
            }
        });
    });
    $("[name='image']").change(function () {
        readURL(this, function (e) {
            $('[image]').attr('src', e.target.result);
        });
    });
}(jQuery, new globalData));






















